name := "HelloSpark"

version := "0.1"

scalaVersion := "2.12.8"

libraryDependencies += "org.apache.spark" %% "spark-sql" % "2.4.4"

libraryDependencies += "au.com.bytecode" % "opencsv" % "2.4"

libraryDependencies += "org.apache.spark" %% "spark-avro" % "2.4.4"

libraryDependencies += "org.apache.spark" % "spark-sql-kafka-0-10_2.12" % "2.4.4"

libraryDependencies += "org.elasticsearch" % "elasticsearch-hadoop" % "7.3.2"

