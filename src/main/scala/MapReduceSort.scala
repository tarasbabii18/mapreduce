import org.apache.spark.sql.types.{DoubleType, IntegerType, StringType, StructType}
import org.apache.spark.sql.SparkSession

object MapReduceSort {
  def main(args: Array[String]): Unit = {
    val files = Array(
            "D:\\BigData\\expediaJSON"
          )
    val spark = SparkSession
      .builder()
      .master("local[*]")
      .appName("MapReduceSort")
      .getOrCreate

    spark.sparkContext.setLogLevel("ERROR")

    val schemaForHotels = (new StructType).add("channel", (new StructType()).add("int", StringType))
      .add("date_time", (new StructType()).add("string", StringType))
      .add("hotel_id", (new StructType()).add("long", StringType))
      .add("id", (new StructType()).add("long", DoubleType))
      .add("is_mobile", (new StructType()).add("int", StringType))
      .add("is_package", (new StructType()).add("int", StringType))
      .add("orig_destination_distance", (new StructType()).add("double", StringType))
      .add("posa_continent", (new StructType()).add("int", StringType))
      .add("site_name", (new StructType()).add("int", StringType))
      .add("srch_adults_cnt", (new StructType()).add("int", IntegerType))
      .add("srch_children_cnt", (new StructType()).add("int", IntegerType))
      .add("srch_ci", (new StructType()).add("string", StringType))
      .add("srch_co", (new StructType()).add("string", StringType))
      .add("srch_destination_id", (new StructType()).add("int", StringType))
      .add("srch_destination_type_id", (new StructType()).add("int", StringType))
      .add("srch_rm_cnt", (new StructType()).add("int", StringType))
      .add("user_id", (new StructType()).add("int", StringType))

     val hotels = spark.read.schema(schemaForHotels).json(files(0))
    hotels.createOrReplaceTempView("hotels")

    val sortedDataSet = spark.sql("SELECT channel, hotel_id, srch_ci, id, srch_adults_cnt FROM hotels " +
      "WHERE srch_adults_cnt.int>1 ORDER BY hotel_id, srch_ci, id")
    sortedDataSet.createOrReplaceTempView("sortedDataSet")

    sortedDataSet.show(100)

    spark.stop()
  }
}
